package com.example.testapplication

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.testapplication.base.activities.MainActivity

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class SampleTest {
    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("com.example.testapplication", appContext.packageName)
    }
    @Test
    fun isActivityShowing(){
        val activity= ActivityScenario.launch(MainActivity::class.java)
        onView(withId(R.id.main))
            .check(matches(isDisplayed()))
    }

    @Test
    fun isToolbarVisible(){
        val activity= ActivityScenario.launch(MainActivity::class.java)

        onView(withId(R.id.toolbarxx))
            .check(matches(isDisplayed()))
        onView(withId(R.id.toolbarxx))
            .check(matches(withEffectiveVisibility(Visibility.VISIBLE)))
    }

    @Test
    fun isTitleVisible(){
        val activity= ActivityScenario.launch(MainActivity::class.java)

        onView(withId(R.id.toolbar_app_title))
            .check(matches(isDisplayed()))
        onView(withId(R.id.toolbarxx))
            .check(matches(withEffectiveVisibility(Visibility.VISIBLE)))
    }

    @Test
    fun isSwipeRefreshing(){
        val activity= ActivityScenario.launch(MainActivity::class.java)

        onView(withId(R.id.swipe_refresh_layout))
            .check(matches(isDisplayed()))
        onView(withId(R.id.toolbarxx))
            .check(matches(withEffectiveVisibility(Visibility.VISIBLE)))
    }

    @Test
    fun isNoDataAvailable(){
        val activity= ActivityScenario.launch(MainActivity::class.java)

        onView(withId(R.id.activityMessage))
            .check(matches(isDisplayed()))
        onView(withId(R.id.toolbarxx))
            .check(matches(withEffectiveVisibility(Visibility.VISIBLE)))
    }
}
