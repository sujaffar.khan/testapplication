package com.example.testapplication


import com.example.testapplication.support.NetworkService
import io.reactivex.Observable
import retrofit2.http.GET
import java.net.NetworkInterface
import java.util.*

interface DataApi {
    companion object : NetworkService<DataApi>("https://dl.dropboxusercontent.com/", DataApi::class.java)

    @GET("s/2iodh4vg0eortkl/facts.json")
    fun getApiData():Observable<Data.Response>
}