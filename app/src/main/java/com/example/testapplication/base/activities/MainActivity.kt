package com.example.testapplication.base.activities

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.testapplication.*

import kotlinx.android.synthetic.main.activity_mvvm_main.*
import kotlinx.android.synthetic.main.common_app_bar_with_title.*
import kotlinx.android.synthetic.main.common_container_with_app_bar.activityMessage
import kotlinx.android.synthetic.main.custom_row_item.view.*
import org.jetbrains.anko.dimen
import org.jetbrains.anko.toast
import kotlin.collections.ArrayList

class MainActivity : BaseViewModelActivity<DataViewModel>(){
    companion object {
        var API_RES:String="";
    }
    public val getAllDataResponse = Observer<Outcome<Data.Response>>{
        when(it){
            is Outcome.Progress -> isProcessing(it.loading)
            is Outcome.Failure -> showMessage(it.e.getMessage())
            is Outcome.Success -> {
               Log.e("GetAllResponse", ""+it.data)
                toolbar_app_title.setText(it.data.title)
                swipe_refresh_layout.isRefreshing = false
                loadList(it.data.rowsList)
            }
        }
    }

    override fun getLayout(): Int = R.layout.activity_mvvm_main

    override fun getMenu(): Int = 0

    override fun bindViews(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            mViewModel = ViewModelProviders.of(this).get(DataViewModel::class.java)
            if(!hasConnection()){
                updateEmptyActivity(
                    R.string.msg_no_internet,
                    R.drawable.ic_portable_wifi_off_white_24dp
                )
            }
            else
            {
                mViewModel.getAllData()
                mViewModel.allDataOutcome.removeObserver(getAllDataResponse)
                mViewModel.allDataOutcome.reObserve(this, getAllDataResponse)

                Log.d("HGH", getAllDataResponse.toString())
                API_RES =getAllDataResponse.toString();


            }

            data_recycler.apply {
                setHasFixedSize(true)
                adapter =
                    DataAdapter(context!!)
                layoutManager = LinearLayoutManager(context)
            }

            swipe_refresh_layout.setOnRefreshListener {
                if(!hasConnection()){
                    updateEmptyActivity(
                        R.string.msg_no_internet,
                        R.drawable.ic_portable_wifi_off_white_24dp
                    )
                }
                else
                {
                    mViewModel.getAllData()
                    mViewModel.allDataOutcome.removeObserver(getAllDataResponse)
                    mViewModel.allDataOutcome.reObserve(this, getAllDataResponse)
                }
            }
        }
    }




    override fun isProcessing(isLoading: Boolean) {
       // if (isLoading) commonProgress.show()
       // else commonProgress.gone()
    }

    override fun showMessage(messageResInt: Int) {
        toast(messageResInt)
    }

    private fun updateEmptyActivity(messageResId: Int, iconResId: Int) {
        activityMessage.show()
        activityMessage.iconTop(iconResId)
        activityMessage.text = text(messageResId)
    }

    private fun loadList(list: List<Data.RowsList>){
        if(list.isNotEmpty()){
            getAdapter().addContent(list)
        }
    }

    private fun getAdapter() = data_recycler.adapter as DataAdapter

    private class DataAdapter(
        ctx: Context,
        private val contents: ArrayList<Data.RowsList> = arrayListOf()
    ) : RecyclerView.Adapter<DataAdapter.ViewHolder>(){
        private val size = ctx.dimen(R.dimen._100sdp)
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder
                = ViewHolder(parent.inflate(R.layout.custom_row_item))

        override fun getItemCount(): Int = contents.size

        override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bindContent(contents[position])

        fun addContent(content:List<Data.RowsList>){
            contents.clear()
            contents.addAll(content)
            notifyDataSetChanged()
        }

        inner class ViewHolder(v: View):RecyclerView.ViewHolder(v){
            fun bindContent(content: Data.RowsList){
                Glide.with(itemView)
                    .load(content.imageHref)
                    .placeholder(R.drawable.img_placeholder)
                    .error(R.drawable.img_broken_placeholder)
                    .override(size, size)
                    .centerCrop()
                    .into(itemView.image)

                itemView.title.text = content.title
                itemView.description.text = content.description
            }
        }
    }

}
