package com.example.testapplication

import android.os.Bundle
import androidx.lifecycle.Observer
import com.example.testapplication.base.vm.BaseViewModel


abstract class  BaseViewModelActivity<V : BaseViewModel> : BaseActivity(){
    private val mLoadingObserver = Observer<Boolean>{ isProcessing(it) }

    private val mMessageObserver = Observer<Int> { showMessage(it) }

    protected lateinit var mViewModel: V

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel.dataLoading.observe(this, mLoadingObserver)
        mViewModel.dataMessage.observe(this, mMessageObserver)
    }

    abstract fun isProcessing(isLoading: Boolean)

    abstract fun showMessage(messageResInt: Int)

}