package com.example.testapplication.base.vm

import android.app.Application
import androidx.annotation.StringRes
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import io.reactivex.disposables.CompositeDisposable

open class BaseAndroidViewModel(context: Application) : AndroidViewModel(context) {
    private val compositeDisposable = CompositeDisposable()

    val dataLoading = MutableLiveData<Boolean>()

    val dataMessage = MutableLiveData<Int>()

    override fun onCleared() {
        super.onCleared()
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.clear()
            compositeDisposable.dispose()
        }
    }

    protected fun getCompositeDisposal() = compositeDisposable

    protected fun isProcessing(isLoading: Boolean) {
        dataLoading.value = isLoading
    }

    protected fun setMessage(@StringRes messageResId: Int) {
        dataMessage.value = messageResId
    }

}