package com.example.testapplication

import android.content.pm.ActivityInfo
import android.os.Build
import android.view.View
import androidx.annotation.*
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import org.jetbrains.anko.dimen
import org.jetbrains.anko.toast

fun Fragment.text(@StringRes resId: Int) = getString(resId)

fun Fragment.quantityText(@PluralsRes resId: Int, quantity: Int): CharSequence =
    resources.getQuantityText(resId, quantity)

fun Fragment.toast(@NonNull message: String) {
    context?.toast(message)
}

fun Fragment.toast(@StringRes messageResId: Int) {
    context?.toast(messageResId)
}

fun Fragment.dimen(@DimenRes dimenResId: Int) = context!!.dimen(dimenResId)

fun Fragment.color(@ColorRes colorResId: Int): Int = ContextCompat.getColor(context!!, colorResId)

fun Fragment.landscape() {
    // Change the video playing orientation
    activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
}

fun Fragment.portrait() {
    activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
}

fun <T : View> Fragment.inflate(@LayoutRes layoutResId: Int): T? = context?.inflate(layoutResId)


fun DialogFragment.showDialog(ctx: FragmentActivity, tag: String = javaClass.simpleName) {
    show(ctx.supportFragmentManager, tag)
}

inline fun Fragment.supportM(block: () -> Unit) {
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
        block()
    }
}