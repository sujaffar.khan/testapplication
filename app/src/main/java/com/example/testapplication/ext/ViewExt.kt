package com.example.testapplication

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import com.google.android.material.button.MaterialButton
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import java.util.concurrent.TimeUnit

val View.ctx: Context
    get() = context

fun View.show() {
    if (visibility != View.VISIBLE) visibility = View.VISIBLE
}

fun View.gone() {
    if (visibility != View.GONE) visibility = View.GONE
}

fun View.invisible() {
    if (visibility != View.INVISIBLE) visibility = View.INVISIBLE
}

fun MaterialButton.backgroundColor(color: Int) {
    backgroundTintList = ColorStateList(
        arrayOf(intArrayOf()),
        intArrayOf(color)
    )
}

fun TextView.iconLeft(@DrawableRes iconResId: Int) {
    setCompoundDrawablesWithIntrinsicBounds(iconResId, 0, 0, 0)
}

fun TextView.iconLeft(icon: Drawable?) {
    setCompoundDrawablesWithIntrinsicBounds(icon, null, null, null)
}

fun TextView.iconRight(@DrawableRes iconResId: Int) {
    setCompoundDrawablesWithIntrinsicBounds(0, 0, iconResId, 0)
}

fun TextView.iconRight(icon: Drawable?) {
    setCompoundDrawablesWithIntrinsicBounds(null, null, icon, null)
}

fun TextView.iconTop(@DrawableRes iconResId: Int) {
    setCompoundDrawablesWithIntrinsicBounds(0, iconResId, 0, 0)
}

fun TextView.iconTop(icon: Drawable?) {
    setCompoundDrawablesWithIntrinsicBounds(null, icon, null, null)
}

fun View.snack(@StringRes messageResId: Int) {
    Snackbar.make(this, messageResId, Snackbar.LENGTH_SHORT).show()
}

fun View.longSnack(@StringRes messageResId: Int) {
    Snackbar.make(this, messageResId, Snackbar.LENGTH_LONG).show()
}

fun View.stringTag(): String = tag as String

fun View.intTag(): Int = tag as Int


fun TextInputLayout.showError(message: String) {
    isErrorEnabled = true
    error = message
}

fun TextInputLayout.hideError() {
    if (isErrorEnabled) {
        error = null
        isErrorEnabled = false
    }
}

fun ViewGroup.lastChild(): View? =
    if (childCount > 0) getChildAt(childCount - 1)
    else null

fun TextView.getEntry(): String = text.trim().toString()

fun TextView.isEmpty(@StringRes messageResId: Int = 0): Boolean {
    if (text.trim().isEmpty()) {
        if (messageResId != 0) error = ctx.text(messageResId)
        requestFocus()
        return true
    }
    return false
}

fun ViewGroup.inflate(@LayoutRes layoutResId: Int): View =
    LayoutInflater.from(context).inflate(layoutResId, this, false)