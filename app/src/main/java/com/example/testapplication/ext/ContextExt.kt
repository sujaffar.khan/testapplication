package com.example.testapplication

import android.app.*
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager.FEATURE_CAMERA
import android.content.pm.PackageManager.FEATURE_MICROPHONE
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.annotation.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import org.jetbrains.anko.*

val Activity.ctx: Application
    get() = application

val Context.connectivityManager: ConnectivityManager
    get() = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

val Context.inputManager: InputMethodManager
    get() = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

val Context.downloadManager: DownloadManager
    get() = getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager

val Context.notificationManager: NotificationManager
    get() = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

val Context.activityManager: ActivityManager
    get() = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager

val Context.screenWidth: Int
    get() = resources.displayMetrics.widthPixels

val Context.screenHeight: Int
    get() = resources.displayMetrics.heightPixels

inline fun supportBelowLollipop(block: () -> Unit) {
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
        block()
    }
}

fun FragmentActivity.findFragmentById(@IdRes fragmentId: Int) = supportFragmentManager.findFragmentById(fragmentId)

fun FragmentActivity.findFragmentByTag(@NonNull fragmentTag: String) =
    supportFragmentManager.findFragmentByTag(fragmentTag)

fun FragmentActivity.replaceFragment(@IdRes idRes: Int, fragment: Fragment, backStack: Boolean = false) {
    val transaction = supportFragmentManager.beginTransaction()
    transaction.replace(idRes, fragment)
    if (backStack) transaction.addToBackStack(null)
    transaction.commit()
}

fun FragmentActivity.addFragment(@IdRes idRes: Int, fragment: Fragment) {
    supportFragmentManager.beginTransaction()
        .add(idRes, fragment).commit()
}


fun <T> Context.getPrefs(@NonNull name: String, @NonNull key: String, default: T) =
    findPreference(getSharedPreferences(name, Context.MODE_PRIVATE), key, default)

fun <T> Context.getPrefs(@NonNull name: String, @StringRes keyRes: Int, default: T) =
    findPreference(getSharedPreferences(name, Context.MODE_PRIVATE), text(keyRes), default)

fun <T> Context.putPrefs(@NonNull name: String, @NonNull key: String, value: T) =
    putPreference(getSharedPreferences(name, Context.MODE_PRIVATE), key, value)

fun <T> Context.putPrefs(@NonNull name: String, @StringRes keyRes: Int, value: T) =
    putPreference(getSharedPreferences(name, Context.MODE_PRIVATE), text(keyRes), value)

fun <T> Context.getDefaultPrefs(@StringRes keyRes: Int, default: T) =
    findPreference(defaultSharedPreferences, text(keyRes), default)

fun <T> Context.putDefaultPrefs(@StringRes keyResId: Int, value: T) =
    putPreference(defaultSharedPreferences, text(keyResId), value)

fun <T> Context.getDefaultPrefs(key: String, default: T) =
    findPreference(defaultSharedPreferences, key, default)

fun <T> Context.putDefaultPrefs(key: String, value: T) =
    putPreference(defaultSharedPreferences, key, value)

@Suppress("UNCHECKED_CAST")
fun <T : View> Context.inflate(@LayoutRes layoutResId: Int, parent: ViewGroup? = null): T =
    LayoutInflater.from(this).inflate(layoutResId, parent) as T


fun Context.text(@StringRes resId: Int): String = getString(resId)

fun Context.array(@ArrayRes resId: Int): Array<String> = resources.getStringArray(resId)

fun Context.color(@ColorRes colorResId: Int): Int = ContextCompat.getColor(this, colorResId)

fun Context.asList(@ArrayRes resId: Int) = resources.getStringArray(resId).asList()

fun Context.drawable(@DrawableRes resId: Int) = ContextCompat.getDrawable(this, resId)

/**
 * Return app settings [Intent]
 */
fun Context.appDeviceSettingsIntent() = Intent().apply {
    action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
    data = Uri.fromParts("package", packageName, null)
}

fun Context.clearPreference(@NonNull name: String) {
    getSharedPreferences(name, Context.MODE_PRIVATE).edit().clear().apply()
}

fun FragmentActivity.hideFragment(@NonNull fragment: Fragment) {
    supportFragmentManager.beginTransaction().hide(fragment).commit()
}

fun FragmentActivity.showFragment(@NonNull fragment: Fragment) {
    supportFragmentManager.beginTransaction().show(fragment).commit()
}

fun Context.hasConnection(): Boolean {
    var isConnected = false
    val activeNetwork: NetworkInfo? = connectivityManager.activeNetworkInfo
    if (activeNetwork != null && activeNetwork.isConnected)
        isConnected = true
    return isConnected
}

fun Context.hasInternet(): Boolean = if (hasConnection()) {
    try {
        val command = "ping -c 1 google.com"
        val hasInternet = Runtime.getRuntime().exec(command).waitFor() == 0
        hasInternet
    } catch (ex: Exception) {
        false
    }
} else false

fun Context.openSettings() = try {
    startActivity(Intent().apply {
        action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
        data = Uri.fromParts("package", packageName, null)
    })
} catch (activityNotFound: Exception) {
    toast("Device settings not found")
}

fun Context.tint(@DrawableRes iconResId: Int, @ColorInt color: Int): Drawable? {
    drawable(iconResId)?.run {
        val drawable = DrawableCompat.wrap(this)
        DrawableCompat.setTint(drawable, color)
        DrawableCompat.setTintMode(drawable, PorterDuff.Mode.SRC_ATOP)
        return drawable
    } ?: return null
}

fun AppCompatActivity.flatActionBar(displayHome: Boolean = true, removeTitle: Boolean = false) {
    supportActionBar?.let { actionBar ->
        actionBar.setDisplayHomeAsUpEnabled(displayHome)
        actionBar.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        actionBar.elevation = 0F
        if (removeTitle) actionBar.title = ""
    }
}

fun Context.hideKeyBoard(v: View) {
    inputManager.hideSoftInputFromWindow(v.windowToken, 0)
}

fun Context.showKeyboard(v: View) {
    if (v.requestFocus()) inputManager.showSoftInput(v, InputMethodManager.SHOW_IMPLICIT)
}

fun Activity.landscape() {
    // Change the video playing orientation
    requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
}

fun Activity.portrait() {
    requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
}

fun Activity.toggleSystemBar() {
    // The UI options currently enabled are represented by a bitfield.
    // getSystemUiVisibility() gives us that bitfield.

    val uiOptions = window.decorView.systemUiVisibility
    var newUiOptions = uiOptions

    // Toggle ui flags
    // val isImmersiveModeEnabled = uiOptions or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY == uiOptions

    // if (isImmersiveModeEnabled) info { "Turning immersive mode mode off" }
    // else info("Turning immersive mode mode on.")

    // Navigation bar hiding:  Backwards compatible to ICS.
    newUiOptions = newUiOptions xor View.SYSTEM_UI_FLAG_HIDE_NAVIGATION

    // Status bar hiding: Backwards compatible to Jellybean
    newUiOptions = newUiOptions xor View.SYSTEM_UI_FLAG_FULLSCREEN

    /*
    * Immersive mode: Backward compatible to KitKat.
    * Note that this flag doesn't do anything by itself, it only augments the behavior of
    * HIDE_NAVIGATION and FLAG_FULLSCREEN.  For the purposes of this fragment_ruskin_contest all three flags
    * are being toggled together.
    *
    * Note that there are two immersive mode UI flags, one of which is referred to as "sticky".
    * Sticky immersive mode differs in that it makes the navigation and status bars
    * semi-transparent, and the UI flag does not get cleared when the user interacts with
    * the screen.
    * */
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
        newUiOptions = newUiOptions xor View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
    }

    window.decorView.systemUiVisibility = newUiOptions
}


inline fun Context.hasCamera(): Boolean = packageManager.hasSystemFeature(FEATURE_CAMERA)
inline fun Context.hasMicrophone(): Boolean = packageManager.hasSystemFeature(FEATURE_MICROPHONE)



