package com.example.testapplication

import androidx.annotation.NonNull
import io.reactivex.Scheduler
import org.jetbrains.annotations.NotNull

interface BaseSchedulerProvider {

    @NotNull
    fun computation(): Scheduler

    @NonNull
    fun io(): Scheduler

    @NonNull
    fun ui(): Scheduler

}