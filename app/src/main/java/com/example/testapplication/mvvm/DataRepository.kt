package com.example.testapplication

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import timber.log.Timber


class DataRepository private constructor() : ResponseDataSource {
    companion object{
        private var INSTANCE : ResponseDataSource? =null

        @JvmStatic
        fun getInstance()=
                INSTANCE
                        ?: synchronized(ResponseDataSource::class.java){
                            INSTANCE
                                    ?: DataRepository()
                                            .also { INSTANCE = it }
                        }
    }

    private val getDataResponseService = DataApi.getInstance()
    override val scheduler: BaseSchedulerProvider = SchedulerProvider()
    override val DataResponse: PublishSubject<Outcome<Data.Response>> = PublishSubject.create()
    override fun getDataResponse(disposable: CompositeDisposable){
        DataResponse.loading(true)
        getDataResponseService.getApiData()
                .performOnBackOutOnMain(scheduler)
                .subscribe({
                    DataResponse.success(it)
                }, {
                    Timber.e(it)
                    DataResponse.failed(it)
                }).addTo(disposable)
    }
}