package com.example.testapplication

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

interface Data {

    @Parcelize
    data class RowsList(
            @SerializedName("title") var title: String,
            @SerializedName("description") var description: String,
            @SerializedName("imageHref") var imageHref: String
    ) : Parcelable

    @Parcelize
    data class Response(
            @SerializedName("title") var title: String,
            @SerializedName("rows") var rowsList: List<RowsList>
    ) : Parcelable


}